package usm.model.repository;

import usm.framework.data.BaseJPARepository;
import usm.model.entity.User;

/**
 *
 * DD Repository for User related actions and events
 *
 * @author: Rushabh Shah
 */
public interface UserRepository extends BaseJPARepository<User, Long> {
    /**
     * Finds a user with the given email
     *
     * @param email
     * @return
     */
    public User findByEmail(String email);
}
