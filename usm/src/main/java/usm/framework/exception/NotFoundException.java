package usm.framework.exception;

/**
 * @author: Rushabh Shah
 */
public class NotFoundException extends Exception {
    public NotFoundException(String message) {
        super(message);
    }
}
