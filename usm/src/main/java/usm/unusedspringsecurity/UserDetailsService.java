package usm.unusedspringsecurity;

/**
 * @author: Rushabh Shah
 */
public interface UserDetailsService extends org.springframework.security.core.userdetails.UserDetailsService {
}
