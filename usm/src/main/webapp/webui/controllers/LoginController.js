/**
 * @Author Rushabh Shah
 */
angular.module('App.Auth')
    .controller('LoginController', ['$scope', '$rootScope', '$location', 'AuthService', LoginController]);

function LoginController($scope, $rootScope, $location, AuthService) {
    var lc = this;

    (function initController() {
        // reset login status
        AuthService.clearCredentials();
    })();

    lc.login = function () {
    	alert('Login called');
        console.log('received the login event for user: '+lc.user.email);
        lc.dataLoading = true;
        $rootScope.isSubmitted = true;
        alert('Authservice called'+lc.user.email);
        AuthService.login(lc.user.email, lc.user.password, function (response) {
        	alert('Response 200');
            if (response.code==200) {
                AuthService.createJWTToken(response.result.user, response.result.token);
                AuthService.setCredentials();
                $location.path('/app');
            } else {
            	alert('Response Else part');
                lc.error = response.result;
                lc.details = response.details;
                lc.dataLoading = false;
                $rootScope.isSubmitted = false;
            }
        });
    };
};