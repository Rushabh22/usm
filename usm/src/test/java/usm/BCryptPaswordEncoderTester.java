package usm;

import org.junit.Before;
import org.junit.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.Assert;

/**
 * @author: Rushabh Shah
 */
public class BCryptPaswordEncoderTester {
    private static BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    
    @Before
    public void setup() {
    }
    
    @Test
    public void testPassword()  throws Exception {
        String encoded = passwordEncoder.encode("Test1234");
        Thread.sleep(5L * 1000L);
        Assert.isTrue(passwordEncoder.matches("Test1234", encoded));
        Assert.isTrue(!passwordEncoder.matches("Test123ss", encoded));
        Assert.isTrue(passwordEncoder.matches("Test1234", encoded));

        System.out.println("Test 1: "+passwordEncoder.matches("Test1234", encoded));
        System.out.println("Test 2: "+passwordEncoder.matches("Test123ss", encoded));
        System.out.println("Test 3: "+passwordEncoder.matches("Test1234", encoded));
    }
}
