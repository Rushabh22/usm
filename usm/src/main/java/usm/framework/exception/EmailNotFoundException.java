package usm.framework.exception;


import org.springframework.security.core.AuthenticationException;

/**
 * @author: Rushabh Shah
 */
public class EmailNotFoundException extends AuthenticationException {
    public EmailNotFoundException(String message) {
        super(message);
    }
}
