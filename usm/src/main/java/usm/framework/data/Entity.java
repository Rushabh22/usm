package usm.framework.data;

/**
 * Proxy interface for all the concrete
 * entities to extend from.
 *
 * @author : Rushabh Shah
 */
public interface Entity {
}
