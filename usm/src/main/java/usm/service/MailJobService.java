package usm.service;

import usm.framework.data.BaseService;
import usm.model.entity.Job;
import usm.model.entity.User;

/**
 * @author: Rushabh Shah
 */
public interface MailJobService extends BaseService<Job, Long> {

    /**
     * Sends the confirmation mail to user.
     *
     * @param user
     */
    public void sendConfirmationMail(User user);
}
